function handle_POST_transactions() {
  ID=${PARAMS["id"]}
  AMOUNT=$(echo "$BODY" | jq -r '.valor')
  TRANSACTION_TYPE=$(echo "$BODY" | jq -r '.tipo')
  DESCRIPTION=$(echo "$BODY" | jq -r '.descricao')

  if [ "$TRANSACTION_TYPE" == "c" ]; then
    OPERATION="+"
  else
    OPERATION="-"
  fi

  if [ ! -z "$ID" ]; then
    QUERY="
INSERT INTO transactions (account_id, amount, description, transaction_type)
VALUES ($ID, $AMOUNT, '$DESCRIPTION', '$TRANSACTION_TYPE');

UPDATE balances
SET amount = amount $OPERATION $AMOUNT
WHERE balances.account_id = $ID;

SELECT 
  json_build_object(
    'limite', accounts.limit_amount,
    'saldo', balances.amount
  )
FROM accounts 
LEFT JOIN balances ON balances.account_id = accounts.id
WHERE account_id = $ID"

    RESULT=`psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$QUERY" | tr -d '[:space:]'` 

    if [ ! -z "$RESULT" ]; then

      RESPONSE=$(cat views/bank_statement.jsonr | sed "s/{{data}}/$RESULT/")
    else
      RESPONSE=$(cat views/404.htmlr)
    fi
  fi
}

#init.sql
CREATE TABLE accounts (
	id SERIAL PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	limit_amount INTEGER NOT NULL
);

CREATE TABLE transactions (
	id SERIAL PRIMARY KEY,
	account_id INTEGER NOT NULL,
	amount INTEGER NOT NULL,
	transaction_type CHAR(1) NOT NULL,
	description VARCHAR(10) NOT NULL,
	date TIMESTAMP NOT NULL DEFAULT NOW(),
	CONSTRAINT fk_accounts_transactions_id
		FOREIGN KEY (account_id) REFERENCES accounts(id)
);

CREATE TABLE balances (
	id SERIAL PRIMARY KEY,
	account_id INTEGER NOT NULL,
	amount INTEGER NOT NULL,
	CONSTRAINT fk_accounts_balances_id
		FOREIGN KEY (account_id) REFERENCES accounts(id)
);

DO $$
BEGIN
	INSERT INTO accounts (name, limit_amount)
	VALUES
		('o barato sai caro', 1000 * 100),
		('zan corp ltda', 800 * 100),
		('les cruders', 10000 * 100),
		('padaria joia de cocaia', 100000 * 100),
		('kid mais', 5000 * 100);
	
	INSERT INTO balances (account_id, amount)
		SELECT id, 0 FROM accounts;
END;
$$;