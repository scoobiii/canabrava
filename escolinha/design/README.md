Rabodegalo design


# Requisitos baseado em teste stress

Validação de criação de transações (débitos e créditos)
Status code 200 com dados válidos
Validação dos campos obrigatórios
Validação dos tipos de dados
Validação de valor máximo de transação
Atualização correta do saldo e limite
Consulta de extratos
Status code 200 com dados válidos
Retorno de últimas transações feitas
Consistência do saldo total com o histórico de transações
Consistência do limite
Concorrência de transações simultâneas
Inserção e validação de 25 transações por segundo do mesmo tipo
Verificação do saldo total após a operação
Validação de requisições concorrentes
5 consultas simultâneas do extrato com dados consistentes
Validação de erros
Status code 404 para cliente não encontrado
Status codes 422/400 para dados de transação inválidos
Métricas
Tempo médio de resposta
Taxa de sucesso
Erros por segundo
Com esses requisitos mapeados, precisamos desenhar os casos de teste funcional cobrindo todos os cenários, criar dados de entrada e validações para cada um. Em seguida, implementar os testes unitários em um framework de testes e automatizar a execução. Por fim, rodaremos os testes de carga e performance para validar a escalabilidade sob alta demanda.

Casos de teste funcionais:

Testar obtenção válida de ID do cliente
Testar tratamento de ID de cliente inválido
Testar transação válida (crédito/débito)
Testar valor de transação inválido
Testar tipo de transação inválido
Testar atualização de saldo e histórico na transação
Testar validação de saldo na transação
Testar obtenção de extrato para cliente válido
Testar erro em extrato para cliente inválido
Framework para testes unitários:


Testes Funcionais

Criar dados de teste para clientes válidos e inválidos, valores/tipos de transação, descrições
Testes para criação com sucesso de crédito/débito cobrindo todos os campos e regras de negócio
Testes para falha na criação de crédito/débito devido a dados inválidos ou limites excedidos
Busca de extratos para cliente válido retornar dados e formatos esperados
Busca falha ou retorna vazio para cliente inválido
Testes Unitários

Modularizar código em unidades testáveis como funções para operações específicas
Usar framework de teste unitário como CUnit para chamar funções com inputs diferentes
Validar saídas e erros esperados são retornados
Testes de Concorrência

Simular requisições concorrentes aos endpoints usando threads/processos
Testar integridade de dados quando mesmos recursos são acessados simultaneamente
Validar controles de concorrência e locks previnem inconsistências
Testes de Carga/Estresse

Automatizar testes para simular altos volumes de tráfego ao longo do tempo
Monitorar performance do sistema e APIs para tempos de resposta e erros
Encontrar ponto de ruptura quando sistema não aguenta mais aumento de carga
Identificar gargalos e otimizar configuração conforme necessário para melhorar escalabilidade
Benchmarks de Performance

Definir SLAs para usuários e transações concurrentes por segundo esperados
Testes de performance ajudam a verificar se sistema atende requisitos sob alta utilização

# requisitos baseados em regras de negócios


Baseado nos requisitos fornecidos na documentação da Rinha de Backend, eis os requisitos chaves que precisamos considerar além dos requisitos de teste de carga já mencionados:

Requisitos Funcionais:

A API deve suportar endpoints de criação de transações para crédito e débito
As transações devem validar campos obrigatórios, tipos de dados e regras de negócio
As atualizações no saldo e limite do cliente após a transação devem estar corretas
A API deve suportar endpoint de consulta de extratos para clientes
A resposta da consulta deve incluir o histórico das últimas transações e os totais atuais
As APIs devem retornar códigos de erro adequados para requisições inválidas
Requisitos Não Funcionais:

As APIs devem suportar um nível mínimo de concorrência para transações (25 TPS)
Várias requisições de consulta simultâneas devem retornar dados consistentes
As APIs devem atender às metas de tempo médio de resposta sob carga
O sistema deve ser escalonável para aumentar o throughput com mais recursos
Outras Considerações:

A configuração inicial de dados de clientes é pré-definida para limitar variáveis
A arquitetura mínima de balanceador de carga e duas instâncias de API é definida
Restrições de recursos de 1,5 CPUs e 550MB de memória se aplicam
Os testes validarão contratos de endpoints e tratamento de erros
Os testes de carga validarão performance, concorrência e escalabilidade
Para atender a esses requisitos, precisaremos:

Modelos de entidade e código de acesso a dados para clientes e transações
Código de camada de serviço implementando lógica de transação e consulta
Controladores de API expondo endpoints e validando requisições
Testes cobrindo funcionalidade, validação, casos de erro e concorrência
Configuração de balanceador de carga e instâncias de API conforme arquitetura
Esquema e scripts de inicialização do banco de dados
Configuração Docker respeitando restrições de recursos

Regras de negócio para validação de transações:

Uma transação de débito nunca pode deixar o saldo do cliente menor que seu limite disponível. Por exemplo, um cliente com limite de 1000 (R$ 10) nunca deverá ter o saldo menor que -1000 (R$ -10).

Os valores das transações devem ser números inteiros positivos representando centavos.

O tipo da transação deve ser apenas 'c' para crédito ou 'd' para débito.

A descrição deve ter entre 1 e 10 caracteres.

Campos como valor, tipo e descrição são obrigatórios.

Garantir dados consistentes em consultas simultâneas:

Realizar várias consultas ao extrato ao mesmo tempo para verificar se todos retornam os mesmos dados consistentes (saldo, extrato, limite etc).

Validar dados como saldo total, últimas transações, limite etc são os mesmos em todas as respostas.

Metas de tempo de resposta sob carga:

Não há metas de SLAs especificadas no documento. Para APIs sob carga, o objetivo é que o tempo médio de resposta se mantenha estável e dentro de valores razoáveis (alguns segundos no máximo) conforme a carga aumenta.

Eventualmente o tempo médio pode aumentar levemente, mas não de forma desproporcional ou que indique problemas de capacidade ou falhas nas APIs.


