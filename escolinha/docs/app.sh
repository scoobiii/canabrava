function handle_GET_bank_statement() {
  ID=${PARAMS["id"]}

  if [ ! -z "$ID" ]; then
    QUERY="
WITH ten_transactions AS (
    SELECT * FROM transactions 
    WHERE account_id = $ID 
    ORDER BY date DESC
    LIMIT 10
)
SELECT 
  json_build_object('saldo', json_build_object(
    'total', balances.amount,
    'data_extrato', NOW()::date,
    'limite', accounts.limit_amount,
    'ultimas_transacoes', 
      CASE 
      WHEN COUNT(transactions) = 0 THEN '[]'
      ELSE
        json_agg(
          json_build_object(
            'valor', transactions.amount,
            'tipo', transactions.transaction_type,
            'descricao', transactions.description,
            'realizada_em', transactions.date::date
          )
        )
      END
  ))
FROM accounts
LEFT JOIN balances ON balances.account_id = accounts.id
LEFT JOIN ten_transactions AS transactions ON transactions.account_id = accounts.id
WHERE accounts.id = $ID
GROUP BY accounts.id, balances.amount, accounts.limit_amount"

    RESULT=`psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$QUERY" | tr -d '[:space:]'` 

    if [ ! -z "$RESULT" ]; then

      RESPONSE=$(cat views/bank_statement.jsonr | sed "s/{{data}}/$RESULT/")
    else
      RESPONSE=$(cat views/404.htmlr)
    fi
  fi
}



------------------

#!/bin/bash

declare -A PARAMS

function handleRequest() {
  read -r -a request <<< "$1"

  # Parses the path parameter (integer)
  path_regex='GET /clientes/([0-9]+)/extrato$'
  if [[ "${request[1]}" =~ $path_regex ]]; then
    PARAMS["id"]="${BASH_REMATCH[1]}"
    handle_GET_bank_statement
  elif [[ "${request[1]}" == "POST /clientes/:id/transacoes" ]]; then
    handle_POST_transactions
  else
    handle_not_found
  fi
}

function handle_GET_bank_statement() {
  local id="${PARAMS["id"]}"

  if [[ -n "$id" ]]; then
    local query="
WITH ten_transactions AS (
    SELECT * FROM transactions 
    WHERE account_id = $id 
    ORDER BY date DESC
    LIMIT 10
)
SELECT 
  json_build_object('saldo', json_build_object(
    'total', balances.amount,
    'data_extrato', NOW()::date,
    'limite', accounts.limit_amount,
    'ultimas_transacoes', 
      CASE 
      WHEN COUNT(transactions) = 0 THEN '[]'
      ELSE
        json_agg(
          json_build_object(
            'valor', transactions.amount,
            'tipo', transactions.transaction_type,
            'descricao', transactions.description,
            'realizada_em', transactions.date::date
          )
        )
      END
  ))
FROM accounts
LEFT JOIN balances ON balances.account_id = accounts.id
LEFT JOIN ten_transactions AS transactions ON transactions.account_id = accounts.id
WHERE accounts.id = $id
GROUP BY accounts.id, balances.amount, accounts.limit_amount"

    local result=$(psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$query" | tr -d '[:space:]') 

    if [[ -n "$result" ]]; then
      RESPONSE=$(cat views/bank_statement.jsonr | sed "s/{{data}}/$result/")
    else
      RESPONSE=$(cat views/404.htmlr)
    fi
  fi
}

function handle_POST_transactions() {
  local id="${PARAMS["id"]}"
  local amount=$(echo "$BODY" | jq -r '.valor')
  local transaction_type=$(echo "$BODY" | jq -r '.tipo')
  local description=$(echo "$BODY" | jq -r '.descricao')

  if [[ "$transaction_type" == "c" ]]; then
    local operation="+"
  else
    local operation="-"
  fi

  if [[ -n "$id" ]]; then
    local query="
INSERT INTO transactions (account_id, amount, description, transaction_type)
VALUES ($id, $amount, '$description', '$transaction_type');

UPDATE balances
SET amount = amount $operation $amount
WHERE balances.account_id = $id;

SELECT 
  json_build_object(
    'limite', accounts.limit_amount,
    'saldo', balances.amount
  )
FROM accounts"

    RESPONSE=$(psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$query" | tr -d '[:space:]')
  fi
}

function handle_not_found() {
  RESPONSE=$(cat views/404.htmlr)
}

# Exemplo de uso:
# request="GET /clientes/1/extrato HTTP/1.1"
# handleRequest "$request"

# request="POST /clientes/:id/transacoes HTTP/1.1"
# handleRequest "$request"

# request="GET /outra-rota HTTP/1.1"
# handleRequest "$request"


---------------------------

function handle_not_found() {
  RESPONSE=$(cat views/404.htmlr)
}


-------------------------
function handle_POST_transactions() {
  ID=${PARAMS["id"]}
  AMOUNT=$(echo "$BODY" | jq -r '.valor')
  TRANSACTION_TYPE=$(echo "$BODY" | jq -r '.tipo')
  DESCRIPTION=$(echo "$BODY" | jq -r '.descricao')

  if [ "$TRANSACTION_TYPE" == "c" ]; then
    OPERATION="+"
  else
    OPERATION="-"
  fi

  if [ ! -z "$ID" ]; then
    QUERY="
INSERT INTO transactions (account_id, amount, description, transaction_type)
VALUES ($ID, $AMOUNT, '$DESCRIPTION', '$TRANSACTION_TYPE');

UPDATE balances
SET amount = amount $OPERATION $AMOUNT
WHERE balances.account_id = $ID;

SELECT 
  json_build_object(
    'limite', accounts.limit_amount,
    'saldo', balances.amount
  )
FROM accounts 
LEFT JOIN balances ON balances.account_id = accounts.id
WHERE account_id = $ID"

    RESULT=`psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$QUERY" | tr -d '[:space:]'` 

    if [ ! -z "$RESULT" ]; then

      RESPONSE=$(cat views/bank_statement.jsonr | sed "s/{{data}}/$RESULT/")
    else
      RESPONSE=$(cat views/404.htmlr)
    fi
  fi
}

#init.sql
CREATE TABLE accounts (
	id SERIAL PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	limit_amount INTEGER NOT NULL
);

CREATE TABLE transactions (
	id SERIAL PRIMARY KEY,
	account_id INTEGER NOT NULL,
	amount INTEGER NOT NULL,
	transaction_type CHAR(1) NOT NULL,
	description VARCHAR(10) NOT NULL,
	date TIMESTAMP NOT NULL DEFAULT NOW(),
	CONSTRAINT fk_accounts_transactions_id
		FOREIGN KEY (account_id) REFERENCES accounts(id)
);

CREATE TABLE balances (
	id SERIAL PRIMARY KEY,
	account_id INTEGER NOT NULL,
	amount INTEGER NOT NULL,
	CONSTRAINT fk_accounts_balances_id
		FOREIGN KEY (account_id) REFERENCES accounts(id)
);

DO $$
BEGIN
	INSERT INTO accounts (name, limit_amount)
	VALUES
		('o barato sai caro', 1000 * 100),
		('zan corp ltda', 800 * 100),
		('les cruders', 10000 * 100),
		('padaria joia de cocaia', 100000 * 100),
		('kid mais', 5000 * 100);
	
	INSERT INTO balances (account_id, amount)
		SELECT id, 0 FROM accounts;
END;
$$;
