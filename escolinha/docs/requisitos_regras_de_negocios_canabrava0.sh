
# README.md
# canabrava

```
                           ___.                              
  ____ _____    ____ _____ \_ |______________ ___  _______   
_/ ___\\__  \  /    \\__  \ | __ \_  __ \__  \\  \/ /\__  \  
\  \___ / __ \|   |  \/ __ \| \_\ \  | \// __ \\   /  / __ \_
 \___  >____  /___|  (____  /___  /__|  (____  /\_/  (____  /
     \/     \/     \/     \/    \/           \/           \/ 
```

Uma versão Bash da [rinha do backend 2ª edição](https://github.com/zanfranceschi/rinha-de-backend-2024-q1) 2024/Q1

## Requisitos

* [Docker](https://docs.docker.com/get-docker/)
* [Gatling](https://gatling.io/open-source/), a performance testing tool
* Make (optional)

## Stack

* 2 Bash apps
* 1 PostgreSQL
* 1 NGINX

## Usage

```bash
$ make help

Usage: make <target>
  help                       Prints available commands
  start.dev                  Start the rinha in Dev
  start.prod                 Start the rinha in Prod
  docker.stats               Show docker stats
  health.check               Check the stack is healthy
  stress.it                  Run stress tests
  docker.build               Build the docker image
  docker.push                Push the docker image
```

## Inicializando a aplicação

```bash
$ docker compose up -d nginx

# Ou então utilizando Make...
$ make start.dev
```

Testando a app:

```bash
$ curl -v http://localhost:9999/clientes/1/extrato

# Ou então utilizando Make...
$ make health.check
```

## Unleash the madness

Colocando Gatling no barulho:

```bash
$ make stress.it 
$ open stress-test/user-files/results/**/index.html
```

----

[ASCII art generator](http://www.network-science.de/ascii/)


#estrutura a ser turbinada
canabrava  README-cloudshell.txt
sobrinhosj@cloudshell:~$ tree canabrava/
canabrava/
├── app
│   ├── bank_statement.bash
│   ├── handler.bash
│   ├── not-found.bash
│   └── transactions.bash
├── config
│   ├── init.sql
│   ├── nginx.conf
│   └── postgresql.conf
├── docker-compose.yml
├── Dockerfile
├── LICENSE
├── Makefile
├── netcat.bash
├── README.md
├── stress-test
│   ├── run-test.sh
│   └── user-files
│       └── simulations
│           └── rinhabackend
│               └── RinhaBackendSimulation.scala
└── views
    ├── 404.htmlr
    ├── bank_statement.jsonr
    └── transactions.jsonr


# bank_statement.bash
import datetime
from fastapi import FastAPI, HTTPException, Path
from database.config import database
from database.db_models import clientes, transactions, saldos   # Corrected import
from sqlalchemy import select, desc
import logging

from domain.models import StatementResponse, TransactionRequest

app = FastAPI()

@app.post("/clientes/{client_id}/transacoes", response_model=dict)
async def transacao(client_id: int, request: TransactionRequest):
    async with database.transaction():
        client_query = select([clientes.c.limite, saldos.c.valor.label("saldo")]).select_from(clientes.join(saldos, clientes.c.id == saldos.c.cliente_id)).where(clientes.c.id == client_id).with_for_update()
        client = await database.fetch_one(client_query)
        
        if client is None:
            raise HTTPException(status_code=404, detail="Cliente não encontrado")
        
        new_saldo = client['saldo'] + (request.valor if request.tipo == 'c' else -request.valor)
        if request.tipo == 'd' and new_saldo < -client['limite']:
            raise HTTPException(status_code=422, detail="Saldo insuficiente")
        
        transaction_dict = {
            "cliente_id": client_id,
            "valor": request.valor,
            "tipo": request.tipo,
            "descricao": request.descricao
        }
        await database.execute(transactions.insert().values(**transaction_dict))
        await database.execute(saldos.update().where(saldos.c.cliente_id == client_id).values(valor=new_saldo))

    return {"limite": client['limite'], "saldo": new_saldo}

@app.get("/clientes/{client_id}/extrato", response_model=StatementResponse)
async def extrato(client_id: int = Path(..., gt=0)):
    client_saldos_join = clientes.join(saldos, clientes.c.id == saldos.c.cliente_id)
    client_query = select([clientes.c.limite, saldos.c.valor.label("saldo")]).select_from(client_saldos_join).where(clientes.c.id == client_id)
    
    client_info = await database.fetch_one(client_query)

    if client_info is None:
        raise HTTPException(status_code=404, detail="Cliente não encontrado")

    transactions_query = select(
        [transactions.c.valor, transactions.c.tipo, transactions.c.descricao, transactions.c.realizada_em]
    ).where(transactions.c.cliente_id == client_id).order_by(desc(transactions.c.realizada_em)).limit(10)
    transaction_records = await database.fetch_all(transactions_query)

    ultimas_transacoes = [{
        "valor": transaction["valor"],
        "tipo": transaction["tipo"],
        "descricao": transaction["descricao"],
        "realizada_em": transaction["realizada_em"]
    } for transaction in transaction_records]

    return {
        "saldo": {
            "total": client_info["saldo"],  
            "data_extrato": datetime.datetime.utcnow(),
            "limite": client_info["limite"]
        },
        "ultimas_transacoes": ultimas_transacoes
    }

@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

# handler.bash
#!/bin/bash

declare -A params

function handleRequest() {
  ## Read the HTTP request until \r\n
  while read line; do
    #echo $line
    trline=$(echo $line | tr -d '[\r\n]') ## Removes the \r\n from the EOL

    ## Breaks the loop when line is empty
    [ -z "$trline" ] && break

    ## Parses the headline
    ## e.g GET /clientes/1/extrato HTTP/1.1 -> GET /clientes/1/extrato
    HEADLINE_REGEX='(.*?)\s(.*?)\sHTTP.*?'

    if [[ "$trline" =~ $HEADLINE_REGEX ]]; then
      REQUEST=$(echo $trline | sed -E "s/$HEADLINE_REGEX/\1 \2/")
      echo $REQUEST >&2
      
      ## Parses the path parameter (integer)
      # e.g GET /clientes/1/extrato HTTP/1.1 -> GET /clientes/:id/extrato -> 1
      PATH_PARAMETER_REGEX='(.*?\s\/.*?)\/(.*?)\/(.*?)$'
      if [[ "$REQUEST" =~ $PATH_PARAMETER_REGEX ]]; then
        PARAMS["id"]=$(echo $REQUEST | sed -E "s/$PATH_PARAMETER_REGEX/\2/")
        REQUEST=$(echo $REQUEST | sed -E "s/$PATH_PARAMETER_REGEX/\1\/:id\/\3/")
      fi
    fi

    ## Parses the Content-Length header
    ## e.g Content-Length: 42 -> 42
    CONTENT_LENGTH_REGEX='Content-Length:\s(.*?)'
    [[ "$trline" =~ $CONTENT_LENGTH_REGEX ]] &&
      CONTENT_LENGTH=$(echo $trline | sed -E "s/$CONTENT_LENGTH_REGEX/\1/")
  done

  BODY=""

  ## Read the remaining HTTP request body
  if [ ! -z "$CONTENT_LENGTH" ]; then
    while read -n$CONTENT_LENGTH -t1 line; do
      trline=`echo $line | tr -d '[\r\n]'`
      BODY+="$trline"

      [ -z "$trline" ] && break
    done
  fi

  ## Fixme: This is a hacky way to finish the request body (JSON)
  ## For some weird reason, the last character is being removed
  BODY+="}"

  ## Route request to the response handler
  source ./app/bank_statement.bash
  source ./app/transactions.bash
  source ./app/not-found.bash

  ## Route request to the response handler
  case "$REQUEST" in
    "GET /clientes/:id/extrato")     handle_GET_bank_statement ;;
    "POST /clientes/:id/transacoes") handle_POST_transactions ;;
    *) 			             handle_not_found ;;
  esac

  echo -e "$RESPONSE" > $FIFO_PATH
}

function handle_not_found() {
  RESPONSE=$(cat views/404.htmlr)
}

# not-found.bash
function handle_not_found() {
  RESPONSE=$(cat views/404.htmlr)
}


# transactions.bash

function handle_POST_transactions() {
  ID=${PARAMS["id"]}
  AMOUNT=$(echo "$BODY" | jq -r '.valor')
  TRANSACTION_TYPE=$(echo "$BODY" | jq -r '.tipo')
  DESCRIPTION=$(echo "$BODY" | jq -r '.descricao')

  if [ "$TRANSACTION_TYPE" == "c" ]; then
    OPERATION="+"
  else
    OPERATION="-"
  fi

  if [ ! -z "$ID" ]; then
    QUERY="
INSERT INTO transactions (account_id, amount, description, transaction_type)
VALUES ($ID, $AMOUNT, '$DESCRIPTION', '$TRANSACTION_TYPE');

UPDATE balances
SET amount = amount $OPERATION $AMOUNT
WHERE balances.account_id = $ID;

SELECT 
  json_build_object(
    'limite', accounts.limit_amount,
    'saldo', balances.amount
  )
FROM accounts 
LEFT JOIN balances ON balances.account_id = accounts.id
WHERE account_id = $ID"

    RESULT=`psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$QUERY" | tr -d '[:space:]'` 

    if [ ! -z "$RESULT" ]; then

      RESPONSE=$(cat views/bank_statement.jsonr | sed "s/{{data}}/$RESULT/")
    else
      RESPONSE=$(cat views/404.htmlr)
    fi
  fi
}


# init.sql

CREATE TABLE accounts (
	id SERIAL PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	limit_amount INTEGER NOT NULL
);

CREATE TABLE transactions (
	id SERIAL PRIMARY KEY,
	account_id INTEGER NOT NULL,
	amount INTEGER NOT NULL,
	transaction_type CHAR(1) NOT NULL,
	description VARCHAR(10) NOT NULL,
	date TIMESTAMP NOT NULL DEFAULT NOW(),
	CONSTRAINT fk_accounts_transactions_id
		FOREIGN KEY (account_id) REFERENCES accounts(id)
);

CREATE TABLE balances (
	id SERIAL PRIMARY KEY,
	account_id INTEGER NOT NULL,
	amount INTEGER NOT NULL,
	CONSTRAINT fk_accounts_balances_id
		FOREIGN KEY (account_id) REFERENCES accounts(id)
);

DO $$
BEGIN
	INSERT INTO accounts (name, limit_amount)
	VALUES
		('o barato sai caro', 1000 * 100),
		('zan corp ltda', 800 * 100),
		('les cruders', 10000 * 100),
		('padaria joia de cocaia', 100000 * 100),
		('kid mais', 5000 * 100);
	
	INSERT INTO balances (account_id, amount)
		SELECT id, 0 FROM accounts;
END;
$$;

# nginx.conf
worker_processes auto;

events {
	worker_connections 256;
}

http {
	access_log off;

	upstream api {
		server api1:3000;
		server api2:3000;
	}

	server {
		listen 9999;

		location / {
			proxy_pass http://api;
		}
	}
}

# postgresql.conf
# -----------------------------
# PostgreSQL configuration file
# -----------------------------

listen_addresses = '*'

# RESOURCE USAGE
max_connections = 30

# QUERY TUNING
random_page_cost = 1.1
effective_io_concurrency = 30


#RinhaBackendSimulation.scala
import scala.concurrent.duration._

import scala.util.Random

import util.Try

import io.gatling.commons.validation._
import io.gatling.core.session.Session
import io.gatling.core.Predef._
import io.gatling.http.Predef._


class RinhaBackendSimulation
  extends Simulation {

  def randomClienteId() = Random.between(1, 5 + 1)
  def randomValorTransacao() = Random.between(1, 10000 + 1)
  def randomDescricao() = Random.alphanumeric.take(10).mkString
  def randomTipoTransacao() = Seq("c", "d", "d")(Random.between(0, 2 + 1)) // not used
  def toInt(s: String): Option[Int] = {
    try {
      Some(s.toInt)
    } catch {
      case e: Exception => None
    }
  }

  val validarConsistenciaSaldoLimite = (valor: Option[String], session: Session) => {
    /*
      Essa função é frágil porque depende que haja uma entrada
      chamada 'limite' com valor conversível para int na session
      e também que seja encadeada com com jmesPath("saldo") para
      que 'valor' seja o primeiro argumento da função validadora
      de 'validate(.., ..)'.
      
      =============================================================
      
      Nota para quem não tem experiência em testes de performance:
        O teste de lógica de saldo/limite extrapola o que é comumente 
        feito em testes de performance apenas por causa da natureza
        da Rinha de Backend. Evite fazer esse tipo de coisa em 
        testes de performance, pois não é uma prática recomendada
        normalmente.
    */ 

    val saldo = valor.flatMap(s => Try(s.toInt).toOption)
    val limite = toInt(session("limite").as[String])

    (saldo, limite) match {
      case (Some(s), Some(l)) if s.toInt < l.toInt * -1 => Failure("Limite ultrapassado!")
      case (Some(s), Some(l)) if s.toInt >= l.toInt * -1 => Success(Option("ok"))
      case _ => Failure("WTF?!")
    }
  }

  val httpProtocol = http
    .baseUrl("http://localhost:9999")
    .userAgentHeader("Agente do Caos - 2024/Q1")

  val debitos = scenario("débitos")
    .exec {s =>
      val descricao = randomDescricao()
      val cliente_id = randomClienteId()
      val valor = randomValorTransacao()
      val payload = s"""{"valor": ${valor}, "tipo": "d", "descricao": "${descricao}"}"""
      val session = s.setAll(Map("descricao" -> descricao, "cliente_id" -> cliente_id, "payload" -> payload))
      session
    }
    .exec(
      http("débitos")
      .post(s => s"/clientes/${s("cliente_id").as[String]}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s => s("payload").as[String]))
          .check(
            status.in(200, 422),
            status.saveAs("httpStatus"))
          .checkIf(s => s("httpStatus").as[String] == "200") { jmesPath("limite").saveAs("limite") }
          .checkIf(s => s("httpStatus").as[String] == "200") {
            jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
          }
    )

  val creditos = scenario("créditos")
    .exec {s =>
      val descricao = randomDescricao()
      val cliente_id = randomClienteId()
      val valor = randomValorTransacao()
      val payload = s"""{"valor": ${valor}, "tipo": "c", "descricao": "${descricao}"}"""
      val session = s.setAll(Map("descricao" -> descricao, "cliente_id" -> cliente_id, "payload" -> payload))
      session
    }
    .exec(
      http("créditos")
      .post(s => s"/clientes/${s("cliente_id").as[String]}/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s => s("payload").as[String]))
          .check(
            status.in(200),
            jmesPath("limite").saveAs("limite"),
            jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
          )
    )

  val extratos = scenario("extratos")
    .exec(
      http("extratos")
      .get(s => s"/clientes/${randomClienteId()}/extrato")
      .check(
        jmesPath("saldo.limite").saveAs("limite"),
        jmesPath("saldo.total").validate("ConsistenciaSaldoLimite - Extrato", validarConsistenciaSaldoLimite)
    )
  )

  val saldosIniciaisClientes = Array(
    Map("id" -> 1, "limite" ->   1000 * 100),
    Map("id" -> 2, "limite" ->    800 * 100),
    Map("id" -> 3, "limite" ->  10000 * 100),
    Map("id" -> 4, "limite" -> 100000 * 100),
    Map("id" -> 5, "limite" ->   5000 * 100),
  )

  val criteriosClientes = scenario("validações")
    .feed(saldosIniciaisClientes)
    .exec(
      /*
        Os valores de http(...) essão duplicados propositalmente
        para que sejam agrupados no relatório e ocupem menos espaço.
        O lado negativo é que, em caso de falha, pode não ser possível
        saber sua causa exata.
      */ 
      http("validações")
      .get("/clientes/#{id}/extrato")
      .check(
        status.is(200),
        jmesPath("saldo.limite").ofType[String].is("#{limite}"),
        jmesPath("saldo.total").ofType[String].is("0")
      )
    )
    .exec(
      http("validações")
      .get("/clientes/6/extrato")
      .check(status.is(404))
    )
    .exec(
      http("validações")
      .post("/clientes/1/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": "toma"}"""))
          .check(
            status.in(200),
            jmesPath("limite").saveAs("limite"),
            jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
          )
    )
    .exec(
      http("validações")
      .post("/clientes/1/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "d", "descricao": "devolve"}"""))
          .check(
            status.in(200),
            jmesPath("limite").saveAs("limite"),
            jmesPath("saldo").validate("ConsistenciaSaldoLimite - Transação", validarConsistenciaSaldoLimite)
          )
    )
    .exec(
      http("validações")
      .get("/clientes/1/extrato")
      .check(
        jmesPath("ultimas_transacoes[0].descricao").ofType[String].is("devolve"),
        jmesPath("ultimas_transacoes[0].tipo").ofType[String].is("d"),
        jmesPath("ultimas_transacoes[0].valor").ofType[Int].is("1"),
        jmesPath("ultimas_transacoes[1].descricao").ofType[String].is("toma"),
        jmesPath("ultimas_transacoes[1].tipo").ofType[String].is("c"),
        jmesPath("ultimas_transacoes[1].valor").ofType[Int].is("1")
    )
  )
  .exec(
      http("validações")
      .post("/clientes/1/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1.2, "tipo": "d", "descricao": "devolve"}"""))
          .check(status.in(422))
    )
    .exec(
      http("validações")
      .post("/clientes/1/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "x", "descricao": "devolve"}"""))
          .check(status.in(422))
    )
    .exec(
      http("validações")
      .post("/clientes/1/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": "123456789 e mais um pouco"}"""))
          .check(status.in(422))
    )
    .exec(
      http("validações")
      .post("/clientes/1/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": ""}"""))
          .check(status.in(422))
    )
    .exec(
      http("validações")
      .post("/clientes/1/transacoes")
          .header("content-type", "application/json")
          .body(StringBody(s"""{"valor": 1, "tipo": "c", "descricao": null}"""))
          .check(status.in(422))
    )

  /* 
    Separar créditos e débitos dá uma visão
    melhor sobre como as duas operações se
    comportam individualmente.
  */
  setUp(
    criteriosClientes.inject(
      atOnceUsers(1)
    ).andThen(
      debitos.inject(
        rampUsersPerSec(1).to(220).during(2.minutes),
        constantUsersPerSec(220).during(2.minutes)
      ),
      creditos.inject(
        rampUsersPerSec(1).to(110).during(2.minutes),
        constantUsersPerSec(110).during(2.minutes)
      ),
      extratos.inject(
        rampUsersPerSec(1).to(10).during(2.minutes),
        constantUsersPerSec(10).during(2.minutes)
      )
    )
  ).protocols(httpProtocol)
}

#run-test.sh

GATLING_BIN_DIR=$HOME/gatling/bin
WORKSPACE=$PWD/stress-test

runGatling() {
    $GATLING_BIN_DIR/gatling.sh -rm local -s RinhaBackendSimulation \
        -rd "Rinha de Backend - 2024/Q1: Crébito" \
        -rf $WORKSPACE/user-files/results \
        -sf $WORKSPACE/user-files/simulations
}

startTest() {
    for i in {1..20}; do
        # 2 requests to wake the 2 api instances up :)
        curl --fail http://localhost:9999/clientes/1/extrato && \
        echo "" && \
        curl --fail http://localhost:9999/clientes/1/extrato && \
        echo "" && \
        runGatling && \
        break || sleep 2;
    done
}

startTest

# 404.htmlr
HTTP/1.1 404 NotFound
Content-Type: text/html

<h1>Sorry, not found</h1>


# bank_statement.jsonr
HTTP/1.1 200
Content-Type: application/json

{{data}}

# transactions.jsonr
HTTP/1.1 200
Content-Type: application/json

{{data}}

#docker-compose.yml
services:
  api1: &api
    build: 
      context: .
      target: base
    container_name: api1
    volumes:
      - .:/app
    depends_on:
      - postgres
      - pgbouncer
    command: bash netcat.bash
    deploy:
      resources:
        limits:
          cpus: '0.2'
          memory: '100MB'

  api2: 
    <<: *api
    container_name: api2

  postgres:
    image: postgres
    container_name: postgres
    environment:
      - POSTGRES_PASSWORD=postgres
    ports:
      - 5432:5432
    volumes:
      - ./config/init.sql:/docker-entrypoint-initdb.d/init.sql
      - ./config/postgresql.conf:/etc/postgresql/postgresql.conf
    command: postgres -c config_file=/etc/postgresql/postgresql.conf
    deploy:
      resources:
        limits:
          cpus: '0.7'
          memory: '200MB'

  pgbouncer:
    image: pgbouncer/pgbouncer
    hostname: pgbouncer
    container_name: pgbouncer
    environment:
      - DATABASES_HOST=postgres
      - DATABASES_PORT=5432
      - DATABASES_USER=postgres
      - DATABASES_PASSWORD=postgres
      - DATABASES_DBNAME=postgres
      - DATABASES_POOL_SIZE=100
      - PGBOUNCER_MAX_CLIENT_CONN=100
      - PGBOUNCER_RESERVE_POOL_SIZE=100
      - PGBOUNCER_DEFAULT_POOL_SIZE=100
    depends_on:
      - postgres
    deploy:
      resources:
        limits:
          cpus: '0.2'
          memory: '100MB'

  nginx:
    image: nginx 
    container_name: nginx
    volumes:
      - ./config/nginx.conf:/etc/nginx/nginx.conf:ro
    ports:
      - 9999:9999
    depends_on:
      - api1
      - api2
    deploy:
      resources:
        limits:
          cpus: '0.2'
          memory: '50MB'

#Dockerfile
FROM ubuntu AS base
RUN apt update && apt install -y netcat postgresql-client socat jq
WORKDIR /app

#Makefile
SHELL = /bin/bash
.ONESHELL:
.DEFAULT_GOAL: help

help: ## Prints available commands
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: make \033[36m<target>\033[0m\n"} /^[.a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

start.dev: ## Start the rinha in Dev
	@docker-compose up -d nginx

docker.stats: ## Show docker stats
	@docker stats --format "table {{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}\t{{.MemPerc}}"

health.check: ## Check the stack is healthy
	@curl -v http://localhost:9999/clientes/1/extrato

stress.it: ## Run stress tests
	@sh stress-test/run-test.sh


#netcat.bash
#!/bin/bash

PID=$$ 

## Create the response FIFO
mkdir -p /tmp/pid-$PID
FIFO_PATH=/tmp/pid-$PID/response
rm -f $FIFO_PATH
mkfifo $FIFO_PATH

source ./app/handler.bash

echo 'Listening on 3000...'

## Keep the server running forever
while true; do
  ## 1. wait for FIFO
  ## 2. creates a socket and listens to the port 3000
  ## 3. as soon as a request message arrives to the socket, pipes it to the handleRequest function
  ## 4. the handleRequest function processes the request message and routes it to the response handler, which writes to the FIFO
  ## 5. as soon as the FIFO receives a message, it's sent to the socket
  ## 6. closes the connection (`-N`), closes the socket and repeat the loop
  cat $FIFO_PATH | nc -lN 3000 | handleRequest
done




# Requisitos e regras de negocios (Requisitos funcionais:

Deve haver dois endpoints:

Transações (POST)
Extrato (GET)
No endpoint de transações:

Deve permitir créditos e débitos
Validação dos campos do body
Retornar limite e saldo atualizado
No endpoint de extrato:

Listar saldo total
Listar último extrato
Listar últimas transações
Deve haver 5 clientes cadastrados previamente

Regras de negócio:

Uma transação de débito nunca pode deixar o saldo menor que o limite
Se deixar o saldo inconsistente, deve retornar status 422
Deve checar se o cliente existe, senão retornar 404
Restrições técnicas:

Arquitetura mínima com load balancer e 2 APIs
CPUs total ≤ 1.5 e memória total ≤ 550MB
Banco de dados obrigatório
Pré-teste para checar disponibilidade (40s))

# Especificações do Ambiente de Testes
Abaixo estão os detalhes do servidor (mentira, é meu note mesmo) que rodará os testes.

Docker

$ docker --version
Docker version 25.0.2, build 29cf629
Gatling

# gatling versão 3.10.3
$ java --version
openjdk 21.0.1 2023-10-17
OpenJDK Runtime Environment (build 21.0.1+12-Ubuntu-223.04)
OpenJDK 64-Bit Server VM (build 21.0.1+12-Ubuntu-223.04, mixed mode, sharing)

CPU

$ lscpu                          
Architecture:            x86_64
  CPU op-mode(s):        32-bit, 64-bit
  Address sizes:         39 bits physical, 48 bits virtual
  Byte Order:            Little Endian
CPU(s):                  4
  On-line CPU(s) list:   0-3
Vendor ID:               GenuineIntel
  Model name:            Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz
    CPU family:          6
    Model:               78
    Thread(s) per core:  2
    Core(s) per socket:  2
    Socket(s):           1
    Stepping:            3
    CPU(s) scaling MHz:  94%
    CPU max MHz:         3100,0000
    CPU min MHz:         400,0000
    BogoMIPS:            5199,98
    Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss 
                         ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_
                         tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1
                          sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault
                          epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust 
                         bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm
                          ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d arch_capabilities
Virtualisation features: 
  Virtualisation:        VT-x
Caches (sum of all):     
  L1d:                   64 KiB (2 instances)
  L1i:                   64 KiB (2 instances)
  L2:                    512 KiB (2 instances)
  L3:                    4 MiB (1 instance)
NUMA:                    
  NUMA node(s):          1
  NUMA node0 CPU(s):     0-3
Vulnerabilities:         
  Gather data sampling:  Not affected
  Itlb multihit:         KVM: Mitigation: VMX disabled
  L1tf:                  Mitigation; PTE Inversion; VMX conditional cache flushes, SMT vulnerable
  Mds:                   Mitigation; Clear CPU buffers; SMT vulnerable
  Meltdown:              Mitigation; PTI
  Mmio stale data:       Mitigation; Clear CPU buffers; SMT vulnerable
  Retbleed:              Mitigation; IBRS
  Spec rstack overflow:  Not affected
  Spec store bypass:     Mitigation; Speculative Store Bypass disabled via prctl
  Spectre v1:            Mitigation; usercopy/swapgs barriers and __user pointer sanitization
  Spectre v2:            Mitigation; IBRS, IBPB conditional, STIBP conditional, RSB filling, PBRSB-eIBRS Not affected
  Srbds:                 Mitigation; Microcode
  Tsx async abort:       Not affected

Memória

$ free -h
               total        used        free      shared  buff/cache   available
Mem:            15Gi       4,4Gi       8,7Gi       754Mi       3,4Gi        11Gi
Swap:          2,0Gi          0B       2,0Gi
SO (Ubuntu 23.04)

$ uname -a
Linux 6.2.0-39-generic #40-Ubuntu SMP PREEMPT_DYNAMIC Tue Nov 14 14:18:00 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
