canabrava/
├── app
│   ├── bank_statement.bash  [function handle_GET_bank_statement() {
  ID=${PARAMS["id"]}

  if [ ! -z "$ID" ]; then
    QUERY="
WITH ten_transactions AS (
    SELECT * FROM transactions 
    WHERE account_id = $ID 
    ORDER BY date DESC
    LIMIT 10
)
SELECT 
  json_build_object('saldo', json_build_object(
    'total', balances.amount,
    'data_extrato', NOW()::date,
    'limite', accounts.limit_amount,
    'ultimas_transacoes', 
      CASE 
      WHEN COUNT(transactions) = 0 THEN '[]'
      ELSE
        json_agg(
          json_build_object(
            'valor', transactions.amount,
            'tipo', transactions.transaction_type,
            'descricao', transactions.description,
            'realizada_em', transactions.date::date
          )
        )
      END
  ))
FROM accounts
LEFT JOIN balances ON balances.account_id = accounts.id
LEFT JOIN ten_transactions AS transactions ON transactions.account_id = accounts.id
WHERE accounts.id = $ID
GROUP BY accounts.id, balances.amount, accounts.limit_amount"

    RESULT=`psql -t -h pgbouncer -U postgres -d postgres -p 6432 -c "$QUERY" | tr -d '[:space:]'` 

    if [ ! -z "$RESULT" ]; then

      RESPONSE=$(cat views/bank_statement.jsonr | sed "s/{{data}}/$RESULT/")
    else
      RESPONSE=$(cat views/404.htmlr)
    fi
  fi
}
]
│   ├── create.bash
│   ├── handler.bash
│   ├── not-found.bash
│   └── transactions.bash
├── config
│   ├── init.sql
│   ├── nginx.conf
│   └── postgresql.conf
├── docker-compose.yml [services:
  api1: &api
    build: 
      context: .
      target: base
    container_name: api1
    volumes:
      - .:/app
    depends_on:
      - postgres
      - pgbouncer
    command: bash netcat.bash
    deploy:
      resources:
        limits:
          cpus: '0.2'
          memory: '100MB'

  api2: 
    <<: *api
    container_name: api2

  postgres:
    image: postgres
    container_name: postgres
    environment:
      - POSTGRES_PASSWORD=postgres
    ports:
      - 5432:5432
    volumes:
      - ./config/init.sql:/docker-entrypoint-initdb.d/init.sql
      - ./config/postgresql.conf:/etc/postgresql/postgresql.conf
    command: postgres -c config_file=/etc/postgresql/postgresql.conf
    deploy:
      resources:
        limits:
          cpus: '0.7'
          memory: '200MB'

  pgbouncer:
    image: pgbouncer/pgbouncer
    hostname: pgbouncer
    container_name: pgbouncer
    environment:
      - DATABASES_HOST=postgres
      - DATABASES_PORT=5432
      - DATABASES_USER=postgres
      - DATABASES_PASSWORD=postgres
      - DATABASES_DBNAME=postgres
      - DATABASES_POOL_SIZE=100
      - PGBOUNCER_MAX_CLIENT_CONN=100
      - PGBOUNCER_RESERVE_POOL_SIZE=100
      - PGBOUNCER_DEFAULT_POOL_SIZE=100
    depends_on:
      - postgres
    deploy:
      resources:
        limits:
          cpus: '0.2'
          memory: '100MB'

  nginx:
    image: nginx 
    container_name: nginx
    volumes:
      - ./config/nginx.conf:/etc/nginx/nginx.conf:ro
    ports:
      - 9999:9999
    depends_on:
      - api1
      - api2
    deploy:
      resources:
        limits:
          cpus: '0.2'
          memory: '50MB']
├── Dockerfile
├── escolinha
│   └── README.md
├── LICENSE
├── Makefile
├── netcat.bash
├── README.md
├── stress-test
│   ├── run-test.sh
│   └── user-files
│       └── simulations
│           └── rinhabackend
│               └── RinhaBackendSimulation.scala
└── views
    ├── 404.htmlr
    ├── bank_statement.jsonr
    └── transactions.jsonr