# -----------------------------
# PostgreSQL configuration file
# -----------------------------

listen_addresses = '*'

# RESOURCE USAGE
max_connections = 100

# CHECKPOINTER
checkpoint_completion_target = 0.9    # Define a porcentagem do trabalho que o checkpointer tenta realizar entre pontos de verificação, ajuste conforme necessário
checkpoint_flush_after = 256kB    # Define o limite de buffer após o qual o checkpointer escreve os dados no disco, ajuste conforme necessário
checkpoint_timeout = 0    # Desativa a escrita periódica de pontos de verificação no disco, escrevendo na memória

# BACKGROUND WRITER
bgwriter_delay = 200ms    # Define o atraso entre as gravações feitas pelo background writer, ajuste conforme necessário
bgwriter_lru_maxpages = 100    # Define o número máximo de páginas sujas que o background writer tentará escrever por vez, ajuste conforme necessário
bgwriter_lru_multiplier = 2.0    # Define a quantidade de páginas a serem escritas por vez, ajuste conforme necessário
bgwriter_flush_after = 256kB    # Define o limite de buffer após o qual o background writer escreve os dados no disco, ajuste conforme necessário

# WRITING
fsync = off    # Desativa a confirmação de que os dados foram gravados no disco antes de confirmar uma transação
synchronous_commit = off    # Desativa a espera pela confirmação de que os dados foram gravados no disco antes de confirmar uma transação
full_page_writes = off    # Reduz a quantidade de dados gravados no disco escrevendo páginas de dados modificadas de forma incremental
max_wal_senders = 5    # Aumenta o número máximo de remetentes de log wal para 5
wal_level = minimal

# WALWRITER
wal_writer_delay = 200ms    # Define o atraso entre as gravações feitas pelo walwriter, ajuste conforme necessário
wal_writer_flush_after = 1MB    # Define o limite de buffer após o qual o walwriter escreve os dados no disco, ajuste conforme necessário

# AUTOVACUUM LAUNCHER
autovacuum = on    # Ativa o autovacuum
autovacuum_max_workers = 3    # Define o número máximo de processos de autovacuum
autovacuum_naptime = 1min    # Define o intervalo de tempo entre as verificações de autovacuum
autovacuum_vacuum_scale_factor = 0.2    # Define a porcentagem de tamanho de tabela que aciona uma operação de VACUUM
autovacuum_analyze_scale_factor = 0.1    # Define a porcentagem de tamanho de tabela que aciona uma operação de ANALYZE
autovacuum_vacuum_threshold = 50    # Define o número mínimo de tuplas obsoletas necessárias para acionar uma operação de VACUUM
autovacuum_analyze_threshold = 50    # Define o número mínimo de tuplas obsoletas necessárias para acionar uma operação de ANALYZE

# LOGICAL REPLICATION LAUNCHER
max_replication_slots = 10    # Define o número máximo de slots de replicação

# QUERY TUNING
random_page_cost = 1.0    # Reduz o custo de leitura de páginas aleatórias para 1.0
effective_io_concurrency = 200    # Aumenta a concorrência de E/S efetiva para 200

# MEMORY ESG sem gravação em banco de dados temos menos emissões logo merecemos mais memória né nenem
shared_buffers = 2GB    # Aumenta o tamanho dos buffers compartilhados para 2GB
work_mem = 64MB    # Aumenta a memória de trabalho para 64MB
maintenance_work_mem = 512MB    # Aumenta a memória de manutenção para 512MB
effective_cache_size = 4GB    # Ajusta o tamanho do cache efetivo para 4GB