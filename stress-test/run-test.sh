#!/usr/bin/bash

# Use este script para executar testes locais

RESULTS_WORKSPACE="$(pwd)/load-test/user-files/results"
GATLING_BIN_DIR=$HOME/gatling/3.10.3/bin
GATLING_WORKSPACE="$(pwd)/load-test/user-files"


runGatling() {
    $GATLING_BIN_DIR/gatling.sh -rm local -s RinhaBackendSimulation \
        -rd "Rinha de Backend - 2024/Q1: Crébito" \
        -rf $WORKSPACE/user-files/results \
        -sf $WORKSPACE/user-files/simulations
}

startTest() {
    for i in {1..20}; do
        # 2 requests to wake the 2 api instances up :)
        curl --fail https://9999-cs-fa481776-aedf-42a6-980d-943bca6e762f.cs-us-east1-vpcf.cloudshell.dev/clientes/1/extrato && \
        echo "" && \
        curl --fail https://9999-cs-fa481776-aedf-42a6-980d-943bca6e762f.cs-us-east1-vpcf.cloudshell.dev/clientes/1/extrato && \
        echo "" && \
        runGatling && \
        break || sleep 2;
    done
}

startTest


